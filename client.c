#include <string.h>
#include <stdio.h>  
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

int
main()
{
 int sock;

 char buf[2048];
 char input[2048];
 char input2[2048];
 char from_addrstr[16];
 char *end = "(bye)";

 struct sockaddr_in addr;
 struct sockaddr_in from_addr;

 socklen_t addrlen;

 /*ソケットを生成*/
 sock = socket(AF_INET, SOCK_DGRAM, 0);

 /*アドレス・ポートの設定*/
 addr.sin_family = AF_INET;
 addr.sin_port = htons(6000);
 addr.sin_addr.s_addr = inet_addr("127.0.0.1");

 /*コマンド入力*/
 scanf("%[^\n]",input);

 /*メッセージを転送*/
 sendto(sock, input, strlen(input), 0, (struct sockaddr *)&addr, sizeof(addr));

 /*バインドする*/
 bind(sock, (struct sockaddr *)&addr, sizeof(addr));

 while(strcmp(input2, end) != 0)
   {

 /*受信バッファの初期化*/
 memset(buf, 0, sizeof(buf));

 addrlen = sizeof(from_addr);

 /*メッセージの受信*/
 recvfrom(sock, buf, sizeof(buf), 0, (struct sockaddr *)&from_addr, &addrlen);

 /*送信元のアドレス・ポートを表示*/
 inet_ntop(AF_INET, &from_addr.sin_addr, from_addrstr, sizeof(from_addrstr));
 printf("recvfrom : %s, port = %d\n", from_addrstr, ntohs(from_addr.sin_port));

 /*受信した文字列の表示*/
 printf("%s\n", buf);

 /*文字の入力*/
 scanf("%s", input2);

 /*メッセージ転送*/
 sendto(sock, input2, strlen(input2), 0, (struct sockaddr *)&from_addr, addrlen);

   }

 close(sock);

 return 0;
}
