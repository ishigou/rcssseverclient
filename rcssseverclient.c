#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

int
main()
{
 int sock;
 struct sockaddr_in addr;
 char buf[2048];

 /*ソケットを生成*/
 sock = socket(AF_INET, SOCK_DGRAM, 0);

 /*アドレス・ポートの設定*/
 addr.sin_family = AF_INET;
 addr.sin_port = htons(6000);
 addr.sin_addr.s_addr = inet_addr("127.0.0.1");

 /*メッセージを転送*/
 sendto(sock, "(init ishigou)", 14, 0, (struct sockaddr *)&addr, sizeof(addr));

 /*バインドする*/
 bind(sock, (struct sockaddr *)&addr, sizeof(addr));

 for(;;)
   {
     /*受信バッファの初期化*/
     memset(buf, 0, sizeof(buf));

     /*メッセージの受信*/
     recv(sock, buf, sizeof(buf), 0);

     printf("%s\n", buf);

   }

 close(sock);

 return 0;
}
