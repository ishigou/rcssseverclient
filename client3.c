#include <string.h>
#include <stdio.h>  
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdbool.h>

void* receive(int sock)
{
      char buf[2048];

  for(;;)
    {
      /*受信バッファの初期化*/
      memset(buf, 0, sizeof(buf));

      /*メッセージの受信*/
      recv(sock, buf, sizeof(buf), 0);

      /*受信した文字列の表示*/
      printf("%s\n\n", buf);
    }
}

int
main()
{
  int sock;
  
  char buf[2048];
  char input[2048];
  char input2[2048];
  char from_addrstr[16];
  char *end = "(bye)";
 
  struct sockaddr_in addr;
  struct sockaddr_in from_addr;
  
  socklen_t addrlen;
  
  pthread_t th;
  
  /*ソケットを生成*/
  sock = socket(AF_INET, SOCK_DGRAM, 0);
  
  /*アドレス・ポートの設定*/
  addr.sin_family = AF_INET;
  addr.sin_port = htons(6000);
  addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  
  /*コマンド入力*/
  scanf("%[^\n]",input);

  /*メッセージを転送*/
  sendto(sock, input, strlen(input), 0, (struct sockaddr *)&addr, sizeof(addr));

 /*バインドする*/
  bind(sock, (struct sockaddr *)&addr, sizeof(addr));

  /*受信バッファの初期化*/
  //  memset(buf, 0, sizeof(buf));

  addrlen = sizeof(from_addr);
      
  /*メッセージの受信*/
  recvfrom(sock, buf, sizeof(buf), 0, (struct sockaddr *)&from_addr, &addrlen);

  /*スレッドの作成と起動*/
  pthread_create( &th, NULL, receive, sock );

  /*送信元のアドレス・ポートを表示*/
  inet_ntop(AF_INET, &from_addr.sin_addr, from_addrstr, sizeof(from_addrstr));
  printf("recvfrom : %s, port = %d\n", from_addrstr, ntohs(from_addr.sin_port));

  while(strcmp(input2,end) != 0)/*(bye)で終了*/
    {

  /*文字の入力*/
  scanf("%s", input2);
      
  /*メッセージ転送*/
  sendto(sock, input2, strlen(input2), 0, (struct sockaddr *)&from_addr, sizeof(from_addr));
  
    }

  close(sock);
  
  return 0;

}
