#include <string.h>
#include <stdio.h>  
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <ncurses.h>
#include <stdlib.h>
#include <ctype.h>


/********************************/

char *token[800];
int  ix;

/*seeの値*/
struct see {
  double dist;
  int angle;
};

struct see fc;
struct see fct;
struct see fcb;
struct see gr;
struct see fgrt;
struct see fgrb;
struct see fprc;
struct see fprt;
struct see fprb;
struct see gl;
struct see fglt;
struct see fglb;
struct see fplc;
struct see fplt;
struct see fplb;
struct see frt;
struct see frb;
struct see flt;
struct see flb;
struct see ft0;
struct see ftr10;
struct see ftr20;
struct see ftr30;
struct see ftr40;
struct see ftr50;
struct see ftl10;
struct see ftl20;
struct see ftl30;
struct see ftl40;
struct see ftl50;
struct see fb0;
struct see fbr10;
struct see fbr20;
struct see fbr30;
struct see fbr40;
struct see fbr50;
struct see fbl10;
struct see fbl20;
struct see fbl30;
struct see fbl40;
struct see fbl50;
struct see fr0;
struct see frt10;
struct see frt20;
struct see frt30;
struct see frb10;
struct see frb20;
struct see frb30;
struct see fl0;
struct see flt10;
struct see flt20;
struct see flt30;
struct see flb10;
struct see flb20;
struct see flb30;

void lexer(char *text)
{
  int i, bgn, n;
  
  ix = 0;
  for (i = 0; text[i] != '\0'; ) 
    {
      if (text[i] <= ' ') 
	{
	  i++;
	  continue;
        }
      //else if(text[i] == '(' || text[i] == ')')
      //{
      //  i++;
      //  continue;
      //}
      bgn = i;

      if (isdigit(text[i]) || text[i]=='-')  //数値 
	{             
	  while (isdigit(text[i]) || text[i]=='.' || text[i]=='-') 
	    i++;
        } 
      else if (isalpha(text[i]))   // 識別子または予約語
	{    
	  while (isalpha(text[i]) || text[i]=='_') 
	    i++;
        }
      else if (text[i]=='(' || text[i]==')')
	{
	  while (text[i]=='(' || text[i]==')')
	    i++;
	}
      else  // 演算子および記号
	{                           
	  i++;
	}

        token[ix] = malloc(i-bgn+1);
        strncpy(token[ix], &text[bgn], i-bgn);
        token[ix][i-bgn] = '\0';             // 文字列末尾にヌル文字を付加
        ix++;
    }

  //if(strcmp(token[0], "see")==0)
  //	{
  //	  int a = 0; 
  //	  a = atoi(token[1]);
  //	  printf("see : %d\n", a);
  //	}

      for (n = 0; n < ix; n++) 
	{
	  if(strcmp(token[0], "see")==0)
	    {

	    }
	  printf("%s%s", token[n], (n<ix-1 ? " " : "\n\n"));
	  free(token[n]);     	// mallocで獲得した文字列を解放
	}
}

/********************************/



void* receive(int sock)
{
  char buf[2048];

  for(;;)
    {
      /*受信バッファの初期化*/
      memset(buf, 0, sizeof(buf));
      
      /*メッセージの受信*/
      recv(sock, buf, sizeof(buf), 0);
      
      lexer(buf);
      
      /*受信した文字列の表示*/
      //printf("%s\n\n", buf);
    }
}

int
main()
{
  int sock, h, w;
  
  char buf[2048];
  char buffer[2048];
  char buffer2[2048];
  char input[2048];
  char input2[2048];
  char from_addrstr[16];
  char *end = "(bye)";
 
  struct sockaddr_in addr;
  struct sockaddr_in from_addr;
  
  socklen_t addrlen;
  
  pthread_t th;

  /*ソケットを生成*/
  sock = socket(AF_INET, SOCK_DGRAM, 0);
  
  /*アドレス・ポートの設定*/
  addr.sin_family = AF_INET;
  addr.sin_port = htons(6000);
  addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  
  /*コマンド入力*/
  fgets(buffer, sizeof(buffer), stdin);
  sscanf(buffer, "%[a-zA-Z0-9 ()]", input);

  /*メッセージを転送*/
  sendto(sock, input, strlen(input), 0, (struct sockaddr *)&addr, sizeof(addr));

 /*バインドする*/
  bind(sock, (struct sockaddr *)&addr, sizeof(addr));

  addrlen = sizeof(from_addr);
      
  /*メッセージの受信*/
  recvfrom(sock, buf, sizeof(buf), 0, (struct sockaddr *)&from_addr, &addrlen);

  /*スレッドの作成と起動*/
  pthread_create( &th, NULL, receive, sock );

  /*送信元のアドレス・ポートを表示*/
  inet_ntop(AF_INET, &from_addr.sin_addr, from_addrstr, sizeof(from_addrstr));
  printf("recvfrom : %s, port = %d\n", from_addrstr, ntohs(from_addr.sin_port));

  while(strcmp(input2,end) != 0)/*(bye)で終了*/
    {

  /*文字の入力*/
  fgets(buffer2, sizeof(buffer2), stdin);
  sscanf(buffer2, "%[a-zA-Z0-9 ()-_]", input2);
      
  /*メッセージ転送*/
  sendto(sock, input2, strlen(input2), 0, (struct sockaddr *)&from_addr, sizeof(from_addr));
  
    }

  close(sock);
  
  return 0;

}

